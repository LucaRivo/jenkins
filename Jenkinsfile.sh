 pipeline {
    agent any
    stages {
        stage('Build image') {
            steps {
                echo 'Starting to build docker image'
                                sh 'docker build -t lucardocker/docker:2 .'



                        }
                }
                stage('Push image') {
            steps {
                echo 'Push on registry docker image'
                                withCredentials([string(credentialsId: 'dockerhub', variable: 'dockerpwd')]) {
                                        sh "docker login -u lucardocker -p ${dockerpwd}"
                                }

                                sh 'docker push lucardocker/docker:2'

                  }
                             }
            }
           }

